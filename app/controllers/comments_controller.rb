class CommentsController < ApplicationController
	def create
		@post = Post.find(params[:id])
		@comment = @post.comment.create(comment_params)

    respond_to do |format|
        format.html { redirect_to @post, notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @post }
    end
	end

	def comment_params
		params.require(:comment).permit(:name, :comment)
	end
end
